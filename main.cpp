#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <time.h>
#include <string>
#include <unistd.h>
#include <sstream>
#define SECONDS 60//Tiempo que correrá el programa

using namespace std;
struct node {
  int process;
  int priority;
  int realizando;
  struct node *next;
};

struct node *conductor;

void append(int process, int priority, struct node *root){
   conductor = root;
   //Manda el conductor al ultimo dato de la lista.
   while ( conductor->next != 0){
      conductor = conductor->next;
   }
   /*Creates a node at the end of the list */
   conductor->next = static_cast<node *> (malloc(sizeof(struct node)));

   conductor = conductor->next;
   if ( conductor == 0 )
    {
        printf( "Out of memory" );
        return;
    }
    /* initialize the new memory */
   conductor->next = 0;         
   conductor->process = process;
   conductor->priority = priority;
   conductor->realizando=0;
   return;
}
//Esta funcion agrega de manera prioritaria
//Para el proyecto la prioridad va a ser 1 y 0
void priorityAppend(int process, int priority, struct node *root){
   int found = 0, tempProcess, tempPrio;
   node *ptr;
   struct node *tempConductor;
   //Esta variable funcionara como booleano, si hay un valor cuya prioridad sea menor, el valor de esta sera 1
   conductor = root;
   ptr = conductor->next;
   while ( conductor->next != 0){
      if (conductor->priority > priority){
         //Estoy en el punto en que la prioridad es menor, asi que debo agregar justo antes
         //Como el valor tempConductor, tiene un valor anterior, es ahi donde debo agregar el valor nuevo.
         found = 1;
         break;
      }
      conductor = conductor->next;
      ptr = conductor->next;
   }
   if (found == 1){
      conductor->next = 0;
      tempProcess = conductor->process;	tempPrio = conductor->priority;
      conductor->process = process;
      conductor->priority = priority;
	  conductor->realizando=0;
      
      append(tempProcess,tempPrio,conductor);
      conductor->next = ptr;
      while(conductor->next != 0){
            conductor = conductor->next;
         }
      conductor->next = 0;
   }else{
      /*Creates a node at the end of the list */
   	conductor->next = static_cast<node *> (malloc(sizeof(struct node)));

      conductor = conductor->next;
      if ( conductor == 0 )
       {
          printf( "Out of memory" );
          return;
       }
       /* initialize the new memory */
      conductor->next = 0;         
      conductor->process = process;
      conductor->priority = priority;
	  conductor->realizando=0;
      }
   return;
}

void printList(struct node* root){
   struct node *conductor;
   conductor = root; 
   while (conductor->next != 0){
	      printf("%d/%d->", conductor->process, conductor->priority);
	      conductor = conductor->next;
	   }
         printf("%d/%d->", conductor->process, conductor->priority);
}


void printProcess(int process, int priority, int element){
      if(element == 0){
         printf("proceso \t%i\t agregado a \timpresion \n", process);
         printf("prioridad \t%i\t agregado a \timpresion \n\n", priority);
         return;
      }
      
      if (element == 1){
         printf("proceso \t%i\t agregado a \temail \n", process);
         printf("prioridad \t%i\t agregado a \temail \n\n", priority);
         return;
      }

      if( element == 2){
         printf("proceso \t%i\t agregado a \tES \n", process);
         printf("prioridad \t%i\t agregado a \tES \n\n", priority);
         return;
      }
   }
   
int calcularCargaCPU(struct node *impresion, struct node *email, struct node *ES, int &sumaRealizando, int &secsImpresion, int &secsEmail, int &secsEntrada){
	if (impresion->realizando==1){
	sumaRealizando+=secsImpresion;
	}
	if (email->realizando==1){
	sumaRealizando+=secsEmail;
	}
	if (ES->realizando==1){
	sumaRealizando+=secsEntrada;
	}
	return ((sumaRealizando)/2)*10;

}
void iniciarMayorPrioridad(struct node *impresion, struct node *email, struct node *ES, int &totalRealizando, int &secsImpresion, int &secsEmail, int &secsEntrada){
	int minimo, c, location = 0;
	int arreglo[3], realizando[3];
	arreglo[0]= impresion->priority;
	arreglo[1]= email->priority;
	arreglo[2]= ES->priority;
	
	realizando[0]= impresion->realizando;
	realizando[1]= email->realizando;
	realizando[2]= ES->realizando;
	
	minimo = arreglo[0];

    for ( c = 0 ; c < 3 ; c++ ) 
    {
		/*cout << "\nArreglo "<< c << "= "<< arreglo[c];
		cout << "\nRealizando "<< c << "= "<< realizando[c];
		*/
        if ( arreglo[c] <= minimo && realizando[c]==0) 
        {
			location = c;
        }
    }
	//cout << "\nEscogido= "<< location;
	switch(location){
		case 0:
		secsImpresion= impresion->process;
		impresion->realizando=1;
		totalRealizando++;
		break;
		case 1:
		secsEmail= email->process;
		email->realizando=1;
		totalRealizando++;
		break;
		case 2:
		secsEntrada= ES->process;
		ES->realizando=1;
		totalRealizando++;
		break;
	}
	
}

int main(int argc, char* argv[]){
//Declaracion de variables a usar
	int auxProcess, auxPrio;
	struct node *impresion, *email, *ES;
	int procesos=0, totalRealizando=0, sumaRealizando=0, segundos=0, total=0, i=0;
	int totalImpresion=0, totalEmail=0, totalEntrada=0;
	int secsImpresion=0, secsEmail=0, secsEntrada=0;
	int tImpresion=0, tEmail=0, tEntrada=0;
	int pImpresion=0, pEmail=0, pEntrada=0;
   srand(time(0));
   
   
   impresion = static_cast<node *> (malloc(sizeof(struct node)));
   email = static_cast<node *> (malloc(sizeof(struct node)));
   ES = static_cast<node *> (malloc(sizeof(struct node)));
   
   
   auxProcess = 1+rand()%10;
   auxPrio = (rand() %3)+1;
   impresion->process = auxProcess;
   impresion->priority = auxPrio;
	impresion->realizando=0;
   
   auxProcess = 1+rand()%10;
   auxPrio = (rand() %3)+1;
   email->process = auxProcess;
   email->priority = auxPrio;
   email->realizando=0;
   
   auxProcess = 1+rand()%10;
   auxPrio = (rand() %3)+1;
   ES->process = auxProcess;
   ES->priority = auxPrio;
   ES->realizando=0;

	secsImpresion= impresion->process;
	secsEmail= email->process;
	secsEntrada= ES->process;

   iniciarMayorPrioridad(impresion, email, ES, totalRealizando, secsImpresion, secsEmail, secsEntrada);
   iniciarMayorPrioridad(impresion, email, ES, totalRealizando, secsImpresion, secsEmail, secsEntrada);
   
   //Inicialización de la pantalla
	system("clear");
	cout << "\n\n**** SIMULACION CPU Dual-core *****\n\n";
	
	cout << "Carga del CPU: " << calcularCargaCPU(impresion,email,ES,sumaRealizando,secsImpresion,secsEmail,secsEntrada) << "% \n";
	cout << "Tiempo maximo de ejecucion: ";
	cout << SECONDS;
	cout <<"s\n";
	cout << "Actualmente:                " ;
	cout << total;
	cout <<"s\n\n";
	cout << "Impresion: ";
	printList(impresion);
	cout << "\n";
	cout << "Email:     ";
	printList(email);
	cout << "\n";
	cout << "E/S:       ";
	printList(ES);
	cout << "\n";
	cout << "\nTareas de Impresion atendidas: ";
	cout << totalImpresion;//Tareas totales realizadas
	cout << "\nTareas de Email atendidas: ";
	cout << totalEmail;//Tareas totales realizadas
	cout << "\nTareas de Entrada/Salida atendidas: ";
	cout << totalEntrada;//Tareas totales realizadas
	 fflush( stdout );
	sleep(1);//Realizar Instrucciones pasando 1 segundo.

	while (total!=SECONDS){
		//Si esa posicion tiene asignada una tarea entonces proceder a hacerla
		total++;
		segundos++;
		//cout << "\nTotalRealizado = "<< totalRealizando;
		while(totalRealizando<2){
			iniciarMayorPrioridad(impresion, email, ES, totalRealizando, secsImpresion, secsEmail, secsEntrada);
		}
			if(impresion->process==0 && impresion->next != 0){//Cuando la tarea termine
				totalImpresion++;
				impresion= impresion->next;//Se realiza la siguiente tarea
				totalRealizando--;
			}
			
			if(email->process==0 && email->next != 0){//Cuando la tarea termine
				totalEmail++;
				email= email->next;//Se realiza la siguiente tarea
				totalRealizando--;
			}
			
			if(ES->process==0 && ES->next != 0){//Cuando la tarea termine
				totalEntrada++;
				ES= ES->next;//Se realiza la siguiente tarea
				totalRealizando--;
			}
		
		if(impresion->realizando==1 && impresion->process>0){
			impresion->process--;//Se resta el tiempo a la tarea que se está realizando
		}
		
		if(email->realizando==1 && email->process>0){
			email->process--;//Se resta el tiempo a la tarea que se está realizando
		}
		
		if(ES->realizando==1 && ES->process>0){
			ES->process--;//Se resta el tiempo a la tarea que se está realizando
		}
			
		if (segundos==3){//Cada 3 segundos realizar...
			tImpresion= 1+rand()%10;//Se genera un tiempo aleatorio para cada tarea entre 1-10 segundos
			tEmail= 1+rand()%10;//Se genera un tiempo aleatorio para cada tarea entre 1-10 segundos
			tEntrada= 1+rand()%10;//Se genera un tiempo aleatorio para cada tarea entre 1-10 segundos
			
			pImpresion= 1+rand()%3;//Se genera una prioridad aleatoria para cada tarea entre 1-3
			pEmail= 1+rand()%3;//Se genera una prioridad aleatoria para cada tarea entre 1-3
			pEntrada= 1+rand()%3;//Se genera una prioridad aleatoria para cada tarea entre 1-3
			
			append(tImpresion,pImpresion,impresion);//Se agrega la tarea a la lista de tareas
			append(tEmail,pEmail,email);//Se agrega la tarea a la lista de tareas
			append(tEntrada,pEntrada,ES);//Se agrega la tarea a la lista de tareas
			
			segundos=0;//Se reinicia el tiempo a cero
		}
		fflush( stdout );
		system("clear");
		cout << "\n\n**** SIMULACION CPU Dual-core *****\n\n";
		cout << "Carga del CPU: " << calcularCargaCPU(impresion,email,ES,sumaRealizando,secsImpresion,secsEmail,secsEntrada) << "% \n";
		cout << "Tiempo maximo de ejecucion: ";
		cout << SECONDS;
		cout <<"s\n";
		cout << "Actualmente:                " ;
		cout << total;
		cout <<"s\n\n";
		cout << "Impresion: ";
		printList(impresion);
		cout << "\n";
		cout << "Email:     ";
		printList(email);
		cout << "\n";
		cout << "E/S:       ";
		printList(ES);
		cout << "\n";
		cout << "\nTareas de Impresion atendidas: ";
		cout << totalImpresion;//Tareas totales realizadas
		cout << "\nTareas de Email atendidas: ";
		cout << totalEmail;//Tareas totales realizadas
		cout << "\nTareas de Entrada/Salida atendidas: ";
		cout << totalEntrada;//Tareas totales realizadas
		 fflush( stdout );
		 sumaRealizando=0;
		sleep(1);//Realizar Instrucciones pasando 1 segundo.
	}
   
  
   
   return 0;
}

